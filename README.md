Это тестовое задание в одну известную компанию.

Список требований к компоненту:
1. В выпадающем списке должен быть реализован поиск по элементам.
2. Список должен выпадать по умолчанию вниз если есть место, если нет, то вверх.
3. Если список не раскрыт, то плейсхолдер должен быть по центру. При клике по выпадающему списку, плейсхолдер должен с анимацией подняться вверх, а в поле ввода должен ставиться фокус.
4. Элементы в списке должны быть отсортированы по алфавиту.
5. Для мобильных устройств и планшетов, на touch событие должен появляться нативный выпадающий список.

Интерес здесь представляет компонент Dropdown. Остальной код - бойлерплейт от create-react-app