import React from 'react'
import styled, {css} from 'react-emotion'
import {Scrollbars} from 'react-custom-scrollbars'
import {isMobile} from 'react-device-detect'

const Wrapper = styled('div')`
    width: 310px;
    height: 52px;
    margin: 2rem auto;
    position: relative;
    transition: all .3s ease;
    
    :before {
        transition: all .3s ease;
        pointer-events: none;
    }
    
    ${p => p.placeholder && css`
        :before {
            content: '${p.placeholder}';
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
            left: 1rem;
        }
    `}
        
    ${p => (p.opened || p.filled || p.isMobile) && css`
        :before {
            content: '${p.placeholder}';
            position: absolute;
            top: 0;
            transform: translateY(-50%);
            left: 1rem;
            font-size: 12px;
            background: #fff;
            padding: .5em;
            opacity: ${p.opened && p.direction === Dropdown.DIRECTION.UP ? 0 : 1};
        }
    `}
    
    input, select {
        height: 50px;
        width: 100%;
        outline: none;
        border: 1px solid #5b5b5b;
        border-radius: 8px;
        padding: 0 1em;
        background-color: #fff;
			
        ${p => p.opened && css`
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        `}
        
        ${p => p.opened && p.direction && p.direction === Dropdown.DIRECTION.UP && css`
            border-radius: 0 0 8px 8px;
        `}
        
        ::placeholder {
            display: none;
        }
    }
`

const List = styled('ul')`
    position: absolute;
    top: 50px;
    border: 1px solid #5b5b5b;
    border-top: none;
    border-bottom-left-radius: 8px;
    border-bottom-right-radius: 8px;
    width: 100%;
    max-height: 200px;
    background: #ffffff;
    
    ${p => p.direction && p.direction === Dropdown.DIRECTION.UP && css`
        border: 1px solid #5b5b5b;
        border-bottom: none;
        border-radius: 8px 8px 0 0;
        top: unset;
        bottom: 52px;
    `}
`

const Option = styled('li')`
    display: block;
    line-height: 50px;
    
    :hover {
        background: rgba(255,136,33,0.3);
        cursor: pointer;
    }
    
    :last-child {
        border-bottom: none;
        border-bottom-left-radius: 8px;
        border-bottom-right-radius: 8px;
    }
    
    .search {
        text-decoration: underline;
        color: rgb(255,136,33);
    }
`


class Dropdown extends React.Component {
    static DIRECTION = {
        UP: 'UP',
        DOWN: 'DOWN',
    }

    state = {
        opened: false,
        inputText: '',
        filteredOptions: this.props.options.sort(),
    }

    open = () => {
        this.setState({
            opened: true,
        })
    }

    close = () => {
        this.setState({
            opened: false,
        })
    }

    onBlur = () => {
        setTimeout(this.close, 200)
    }

    getDirection() {
        if (!this.container) {
            return;
        }

        const container = this.container.getBoundingClientRect();
        const listHeight = 500;

        if (container.top + container.height + listHeight < window.innerHeight) {
            return Dropdown.DIRECTION.DOWN
        }
        return Dropdown.DIRECTION.UP;
    }

    onChange = (e) => {
        this.setState({
            inputText: e.currentTarget.value,
            filteredOptions: this.props.options
                .filter(option => ~option.indexOf(e.currentTarget.value))
        })
    }

    onSelect = (e) => {
        this.setState({
            opened: false,
            inputText: isMobile ? e.currentTarget.value : e.currentTarget.getAttribute('name'),
        })
    }

    render() {
        const direction = this.getDirection()
        return (
            <Wrapper
                placeholder={this.props.placeholder}
                filled={!!this.state.inputText}
                opened={this.state.opened}
                isMobile={isMobile}
                innerRef={n => this.container = n}
                direction={direction}
            >
                {isMobile ? (
                    <select onChange={this.onSelect} value={this.state.inputText}>
                        <option key={''} value={''}/>
                        {this.state.filteredOptions.map((option) =>
                            <option key={option} value={option}>{option}</option>
                        )}
                    </select>
                ) : (
                    <React.Fragment>
                        <input
                            type='text'
                            value={this.state.inputText}
                            onClick={this.open}
                            onChange={this.onChange}
                            onBlur={this.onBlur}
                        />
                        {this.state.opened &&
                        <List direction={direction}>
                            <Scrollbars autoHeight autoHeightMax={200}>
                                {this.state.filteredOptions.map((option) => {
                                    const index = option.indexOf(this.state.inputText)
                                    if (~index && this.state.inputText) {
                                        return <Option key={option} name={option} onClick={this.onSelect}>
                                            {option.slice(null, index)}
                                            <span className='search'>
                                                {option.slice(index, index + this.state.inputText.length)}
                                            </span>
                                            {option.slice(index + this.state.inputText.length)}
                                        </Option>
                                    }
                                    return <Option key={option} name={option} onClick={this.onSelect}>{option}</Option>
                                })}
                            </Scrollbars>
                        </List>
                        }
                    </React.Fragment>
                )}
            </Wrapper>
        )
    }
}

export {Dropdown}
