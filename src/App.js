import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import {Dropdown} from './Dropdown'
import countries from './countries'

class App extends Component {
    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h1 className="App-title">Evgeny Cherkasov</h1>
                    <a
                        target="_blank"
                        rel="noopener noreferrer"
                        href="https://gitlab.com/StipJey/iqOptionDropdown/blob/master/src/Dropdown/index.js"
                    >Source code</a>
                </header>
                <div className="App-intro">
                    <Dropdown
                        placeholder="Выберите страну"
                        options={countries}
                    />
                </div>
                <div className="App-footer">
                    <Dropdown
                        placeholder="Выберите страну"
                        options={countries}
                    />
                </div>
            </div>
        );
    }
}

export default App;
